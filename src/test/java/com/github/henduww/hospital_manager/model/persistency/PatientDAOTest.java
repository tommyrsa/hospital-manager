package com.github.henduww.hospital_manager.model.persistency;

import com.github.henduww.hospital_manager.model.Patient;
import org.junit.jupiter.api.*;

import javax.persistence.EntityExistsException;
import javax.persistence.Persistence;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class PatientDAOTest {

    public static PatientDAO testDAO;

    @BeforeAll
    public static void beforeAll() {
        testDAO = new PatientDAO(Persistence.createEntityManagerFactory("pu-derby-test"));
    }

    @Test
    public void constructor_argIsNull_throwsNullPointerException() {
        assertThrows(NullPointerException.class, () -> new PatientDAO(null));
    }

    @Test
    public void getAll_returnsAllEntries() {
        List<Patient> testPatients
            = Arrays.asList(
            new Patient("11111111111", "Frøken", "Test", "", ""),
            new Patient("22222222222", "Endaen", "Test", "", ""),
            new Patient("33333333333", "Ogen", "Til", "", ""));

        testPatients.forEach(patient -> testDAO.create(patient));

        assertEquals(testPatients, testDAO.getAll());
    }

    @Nested
    public class CreateTest {

        @Test
        public void create_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testDAO.create(null));
        }

        @Test
        public void create_argIsNonExistingPatient_createsPatient() {
            Patient testPatient = new Patient("59595959595", "Blå", "Blad", "", "");
            testDAO.create(testPatient);

            assertTrue(testDAO.exists(testPatient.getSocialSecurityNumber()));
        }

        @Test
        public void create_argIsExistingPatient_throwsEntityExistsException() {
            Patient testPatient = new Patient("12312312312", "Tom", "Bla", "", "");
            testDAO.create(testPatient);

            assertThrows(EntityExistsException.class, () -> testDAO.create(testPatient));
        }
    }

    @Nested
    public class GetTest {

        @Test
        public void get_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testDAO.get(null));
        }

        @Test
        public void get_argIsExistingPatient_getsPatient() {
            Patient testPatient = new Patient("52525252525", "Test", "Ja", "", "");
            testDAO.create(testPatient);

            Optional<Patient> getResult = testDAO.get(testPatient.getSocialSecurityNumber());
            if (getResult.isPresent()) {
                assertEquals(testPatient, getResult.get());
            } else {
                fail("Could not retrieve the recently inserted object.");
            }
        }

        @Test
        public void get_argIsNonExistingPatient_returnsEmpty() {
            Optional<Patient> getResult = testDAO.get("22");
            assertTrue(getResult.isEmpty());
        }
    }

    @Nested
    public class ExistsTest {

        @Test
        public void exists_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testDAO.exists(null));
        }

        @Test
        public void exists_argIsExistingPatient_returnsTrue() {
            Patient testPatient = new Patient("89898989898", "Test", "Ja", "", "");
            testDAO.create(testPatient);

            assertTrue(testDAO.exists(testPatient.getSocialSecurityNumber()));
        }

        @Test
        public void exists_argIsNonExistingPatient_returnsFalse() {
            assertFalse(testDAO.exists("22"));
        }
    }

    @Nested
    public class UpdateTest {

        @Test
        public void update_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testDAO.update(null));
        }

        @Test
        public void update_argIsExistingPatient_updatesPatient() {
            Patient testPatient = new Patient("78787878787", "Test", "Ja", "", "");
            testDAO.create(testPatient);

            testPatient.setFirstName("Bytta!");
            boolean methodResult = testDAO.update(testPatient);

            assertTrue(methodResult);
            assertEquals(testPatient, testDAO.get(testPatient.getSocialSecurityNumber()).get());
        }

        @Test
        public void update_argIsNonExistingPatient_returnsFalse() {
            assertFalse(testDAO.update(new Patient("99999999999", "9", "9", "9", "9")));
        }
    }

    @Nested
    public class DeleteTest {

        @Test
        public void delete_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testDAO.delete(null));
        }

        @Test
        public void delete_argIsExistingPatient_deletesPatient() {
            Patient testPatient = new Patient("61616161616", "Test", "Ja", "", "");
            testDAO.create(testPatient);

            boolean methodResult = testDAO.delete(testPatient.getSocialSecurityNumber());

            assertTrue(methodResult);
            assertFalse(testDAO.exists(testPatient.getSocialSecurityNumber()));
        }

        @Test
        public void delete_argIsNonExistingPatient_returnsFalse() {
            assertFalse(testDAO.update(new Patient("99999999999", "9", "9", "9", "9")));
        }
    }
}
