package com.github.henduww.hospital_manager.model.persistency.serialization;

import com.github.henduww.hospital_manager.model.Patient;
import com.github.henduww.hospital_manager.model.persistency.serialization.PatientSerializer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientSerializerTest {

    @Test
    public void constructor_argIsNull_throwsNullPointerException() {
        assertThrows(NullPointerException.class, () -> new PatientSerializer(null));
    }

    @Nested
    public class SerializeTest {

        private final List<Patient> patientsToSerialize  = Arrays.asList(
            new Patient("11111111111", "Frøken", "Test", "", ""),
            new Patient("22222222222", "Endaen", "Test", "", ""),
            new Patient("33333333333", "Ogen", "Til", "", ""));

        private File createTemporaryFile(String fileName, String fileExtension) {
            try {
                File file = File.createTempFile(fileName, fileExtension);
                file.deleteOnExit();

                return file;
            } catch (IOException e) {
                fail("Failed to create temporary file to perform tests.");
                return null;
            }
        }

        @Test
        public void serialize_fileIsNotCSV_throwsIllegalArgumentException() {
            File testJpg = createTemporaryFile("testJpg", ".jpg");

            assertThrows(IllegalArgumentException.class,
                () -> new PatientSerializer(patientsToSerialize).serialize(testJpg));
        }

        @Test
        public void serialize_fileIsLocked_throwsIOException() {
            File testCSV = createTemporaryFile("testCsv", ".csv");

            try {
                final RandomAccessFile raFile = new RandomAccessFile(testCSV, "rw");
                raFile.getChannel().lock();
            } catch (IOException e) {
                fail("Failed to lock file to perform test.");
            }

            assertThrows(IOException.class,
                () -> new PatientSerializer(patientsToSerialize).serialize(testCSV));
        }

        @Test
        public void serialize_fileIsCSV_serializesPatients() {
            final List<String> expectedCSVObjectsStrings = Arrays.asList(
                "DIAGNOSIS;FIRSTNAME;GENERALPRACTITIONER;LASTNAME;SOCIALSECURITYNUMBER",
                ";Frøken;;Test;11111111111",
                ";Endaen;;Test;22222222222",
                ";Ogen;;Til;33333333333"
            );
            File testCSV = createTemporaryFile("testCsv", ".csv");

            try {
                new PatientSerializer(patientsToSerialize).serialize(testCSV);

                List<String> csvObjectStrings = Files.readAllLines(testCSV.toPath());

                assertEquals(expectedCSVObjectsStrings, csvObjectStrings);
            } catch (SecurityException | IOException e) {
                fail("Failed to read from file to perform test.");
            }
        }

        @Test
        public void serialize_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new PatientSerializer(patientsToSerialize).serialize(null));
        }
    }
}
