package com.github.henduww.hospital_manager.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    @Nested
    public class ConstructorTest {

        @Test
        public void ctor_socialSecurityNumberIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new Patient(null, "", "", "", ""));
        }

        @Test
        public void ctor_socialSecurityNumberIsNaN_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                () -> new Patient("Not a valid number", "", "", "", ""));
        }

        @Test
        public void ctor_socialSecurityIsNot11Digits_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                    () -> new Patient("1234", "", "", "", ""));
        }

        @Test
        public void ctor_firstNameIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new Patient("", null, "", "", ""));
        }

        @Test
        public void ctor_lastNameIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new Patient("", "", null, "", ""));
        }

        @Test
        public void ctor_diagnosisIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new Patient("", "", "", null, ""));
        }

        @Test
        public void ctor_generalPractitionerIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new Patient("", "", "", "", null));
        }
    }

    @Nested
    public class GetterTest {

        private final String testSocialSecurityNumber = "12345678910";
        private final String testFirstName = "Fornavn";
        private final String testLastName = "Etternavn";
        private final String testDiagnosis = "Diagnose";
        private final String testGeneralPractitioner = "Dr. Lege";

        private final Patient testPatient
            = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);

        @Test
        public void getSocialSecurityNumber_returnsSocialSecurityNumber() {
            String expected = testSocialSecurityNumber;
            String actual = testPatient.getSocialSecurityNumber();

            assertEquals(expected, actual);
        }

        @Test
        public void getFirstName_returnsFirstName() {
            String expected = testFirstName;
            String actual = testPatient.getFirstName();

            assertEquals(expected, actual);
        }

        @Test
        public void getLastName_returnsLastName() {
            String expected = testLastName;
            String actual = testPatient.getLastName();

            assertEquals(expected, actual);
        }

        @Test
        public void getDiagnosis_returnsDiagnosis() {
            String expected = testDiagnosis;
            String actual = testPatient.getDiagnosis();

            assertEquals(expected, actual);
        }

        @Test
        public void getGeneralPractitioner_returnsGeneralPractitioner() {
            String expected = testGeneralPractitioner;
            String actual = testPatient.getGeneralPractitioner();

            assertEquals(expected, actual);
        }
    }

    @Nested
    public class SetterTest {

        Patient testPatient = new Patient();

        @Test
        public void setFirstName_nullAsArg_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testPatient.setFirstName(null));
        }

        @Test
        public void setFirstName_emptyStringAsArg_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class, () -> testPatient.setFirstName(""));
        }

        @Test
        public void setFirstName_nonEmptyStringAsArg_setsFirstName() {
            String firstNameToSet = "Fornavn";
            testPatient.setFirstName(firstNameToSet);

            assertEquals(firstNameToSet, testPatient.getFirstName());
        }

        @Test
        public void setLastName_nullAsArg_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> testPatient.setLastName(null));
        }

        @Test
        public void setLastName_emptyStringAsArg_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class, () -> testPatient.setLastName(""));
        }

        @Test
        public void setLastName_nonEmptyStringAsArg_setsLastName() {
            String lastNameToSet = "Etternavn";
            testPatient.setLastName(lastNameToSet);

            assertEquals(lastNameToSet, testPatient.getLastName());
        }

        @Test
        public void setDiagnosis_setsDiagnosis() {
            String diagnosisToSet = "Diagnose";
            testPatient.setDiagnosis(diagnosisToSet);

            assertEquals(diagnosisToSet, testPatient.getDiagnosis());
        }

        @Test
        public void setGeneralPractitioner_setsGeneralPractitioner() {
            String generalPractitionerToSet = "Dr. lege";
            testPatient.setGeneralPractitioner(generalPractitionerToSet);

            assertEquals(generalPractitionerToSet, testPatient.getGeneralPractitioner());
        }
    }

    @Nested
    public class equalsTest {

        @Test
        public void equals_equalPatients_returnsTrue() {
            String testSocialSecurityNumber = "12345678910";
            String testFirstName = "Fornavn";
            String testLastName = "Etternavn";
            String testDiagnosis = "Diagnose";
            String testGeneralPractitioner = "Dr. Lege";

            Patient patient1
                = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);
            Patient patient2
                = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);

            assertTrue(patient1.equals(patient2) && patient2.equals(patient1));
        }

        @Test
        public void equals_nonEqualPatients_returnsFalse() {
            Patient patient1
                = new Patient("12345678910", "Fornavn1", "Etternavn1", "Diagnose1", "Dr. Lege");
            Patient patient2
                = new Patient("10987654321", "Fornavn2", "Etternavn2", "Diagnose2", "Dr. Doktor");

            assertFalse(patient1.equals(patient2) && patient2.equals(patient1));
        }

        @Test
        public void equals_nullAsArg_returnsFalse() {
            Patient patient
                = new Patient("12345678910", "Fornavn", "Etternavn", "Diagnose", "Dr. Lege");

            assertFalse(patient.equals(null));
        }
    }

    @Nested
    public class hashCodeTest {

        @Test
        public void hashCode_equalPatients_returnsEqualHashCodes() {
            String testSocialSecurityNumber = "12345678910";
            String testFirstName = "Fornavn";
            String testLastName = "Etternavn";
            String testDiagnosis = "Diagnose";
            String testGeneralPractitioner = "Dr. Lege";

            Patient patient1
                = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);
            Patient patient2
                = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);

            assertEquals(patient1.hashCode(), patient2.hashCode());
        }

        @Test
        public void hashCode_nonEqualPatients_returnsNonEqualHashCodes() {
            Patient patient1
                = new Patient("12345678910", "Fornavn1", "Etternavn1", "Diagnose1", "Dr. Lege");
            Patient patient2
                = new Patient("10987654321", "Fornavn2", "Etternavn2", "Diagnose2", "Dr. Doktor");

            assertNotEquals(patient1.hashCode(), patient2.hashCode());
        }
    }
}
