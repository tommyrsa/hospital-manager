package com.github.henduww.hospital_manager.model.persistency;

import com.github.henduww.hospital_manager.model.Patient;
import org.junit.jupiter.api.*;

import javax.persistence.EntityExistsException;
import javax.persistence.Persistence;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientDBContextTest {

    private static PatientDBContext dbContext;

    private static List<Patient> testPatients;

    @BeforeAll
    public static void beforeAll() {
        dbContext = new PatientDBContext("pu-derby-test");
        testPatients = Arrays.asList(
                new Patient("99999999999", "Frøken", "Test", "", ""),
                new Patient("88888888888", "Endaen", "Test", "", ""),
                new Patient("77777777777", "Ogen", "Til", "", ""));
    }

    @Nested
    public class ConstructorTest {
        @Test
        public void constructor_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> new PatientDBContext(null));
        }

        @Test
        public void constructor_argIsEmpty_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class, () -> new PatientDBContext(""));
        }
    }

    @Test
    public void load_loadsAllData() {
        PatientDAO patientDAO = new PatientDAO(Persistence.createEntityManagerFactory("pu-derby-test"));

        try {
            testPatients.forEach(patientDAO::create);
        } catch (EntityExistsException ignored) {
            // Issue is out of this scope.
        }

        assertEquals(testPatients, dbContext.load());
    }

    @Test
    public void save_argIsNull_throwsNullPointerException() {
        assertThrows(NullPointerException.class, () -> dbContext.save(null));
    }

    @Test
    public void save_argIsPatientList_savesToDb() {
        PatientDAO patientDAO = new PatientDAO(Persistence.createEntityManagerFactory("pu-derby-test"));

        dbContext.save(testPatients);

        boolean oneOrMoreWasNotAdded
            = testPatients
            .stream()
            .anyMatch(patient -> !patientDAO.exists(patient.getSocialSecurityNumber()));

        assertFalse(oneOrMoreWasNotAdded);
    }
}
