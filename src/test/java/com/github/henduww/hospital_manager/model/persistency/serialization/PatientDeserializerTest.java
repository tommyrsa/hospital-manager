package com.github.henduww.hospital_manager.model.persistency.serialization;

import com.github.henduww.hospital_manager.model.Patient;
import com.github.henduww.hospital_manager.model.persistency.serialization.PatientDeserializer;
import com.github.henduww.hospital_manager.model.persistency.serialization.PatientSerializer;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientDeserializerTest {

    private File createTemporaryFile(String fileName, String fileExtension) {
        try {
            File file = File.createTempFile(fileName, fileExtension);
            file.deleteOnExit();

            return file;
        } catch (IOException e) {
            fail("Failed to create temporary file to perform tests.");
            return null;
        }
    }

    @Test
    public void deserialize_fileIsNotCSV_throwsIllegalArgumentException() {
        File testJpg = createTemporaryFile("testJpg", ".jpg");

        assertThrows(IllegalArgumentException.class,
            () -> new PatientDeserializer().deserialize(testJpg));
    }

    @Test
    public void deserialize_fileIsCSV_serializesPatients() {
        final List<Patient> expectedDeserializeResult  = Arrays.asList(
            new Patient("11111111111", "Frøken", "Test", "", ""),
            new Patient("22222222222", "Endaen", "Test", "", ""),
            new Patient("33333333333", "Ogen", "Til", "", ""));

        File testCSV = createTemporaryFile("testCsv", ".csv");
        writeCSVObjectsToFile(testCSV);

        List<Patient> deserializeResult = new PatientDeserializer().deserialize(testCSV);

        assertEquals(expectedDeserializeResult, deserializeResult);
    }

    @Test
    public void deserialize_argIsNull_throwsNullPointerException() {
        assertThrows(NullPointerException.class,
            () -> new PatientDeserializer().deserialize(null));
    }

    private void writeCSVObjectsToFile(File file) {
        try {
            final List<Patient> objectsToWrite  = Arrays.asList(
                new Patient("11111111111", "Frøken", "Test", "", ""),
                new Patient("22222222222", "Endaen", "Test", "", ""),
                new Patient("33333333333", "Ogen", "Til", "", ""));

            new PatientSerializer(objectsToWrite).serialize(file);
        } catch (IOException e) {
            fail("Failed to write to file to perform test.");
        }
    }
}
