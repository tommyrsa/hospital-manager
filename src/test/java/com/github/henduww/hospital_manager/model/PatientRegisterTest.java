package com.github.henduww.hospital_manager.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {

    @Nested
    public class ConstructorTest {

        @Test
        public void defaultCtor_initializesEmptyList() {
            PatientRegister register = new PatientRegister();

            assertEquals(0, register.getPatients().size());
        }

        @Test
        public void parameterizedCtor_nullAsArg_throwsNullPointerException() {
            assertThrows(NullPointerException.class, () -> new PatientRegister(null));
        }

        @Test
        public void parameterizedCtor_listAsArg_setsList() {
            List<Patient> patientList = Arrays.asList(new Patient(), new Patient());

            PatientRegister register = new PatientRegister(patientList);

            assertEquals(patientList, register.getPatients());
        }
    }

    @Nested
    public class addPatientTest {

        @Test
        public void addPatient_nullAsArg_throwsNullPointerException() {
            PatientRegister register = new PatientRegister();

            assertThrows(NullPointerException.class, () -> register.addPatient(null));
        }

        @Test
        public void addPatient_nonExistingPatientAsArg_addsPatient() {
            PatientRegister register = new PatientRegister();
            Patient patientToAdd = new Patient();

            assertTrue(register.addPatient(patientToAdd));
            assertTrue(register.getPatients().contains(patientToAdd));
        }

        @Test
        public void addPatient_existingPatientAsArg_doesNotAddPatient() {
            String testSocialSecurityNumber = "12345678910";
            String testFirstName = "Fornavn";
            String testLastName = "Etternavn";
            String testDiagnosis = "Diagnose";
            String testGeneralPractitioner = "Dr. Lege";

            PatientRegister register = new PatientRegister();
            Patient patient1ToAdd = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);
            Patient patient2ToAdd = new Patient(testSocialSecurityNumber, testFirstName, testLastName, testDiagnosis, testGeneralPractitioner);

            register.addPatient(patient1ToAdd);
            assertFalse(register.addPatient(patient2ToAdd));
        }
    }

    @Nested
    public class removePatientTest {

        @Test
        public void removePatient_nullAsArg_throwsNullPointerException() {
            PatientRegister register = new PatientRegister();

            assertThrows(NullPointerException.class, () -> register.removePatient(null));
        }

        @Test
        public void removePatient_nonExistingPatientAsArg_doesNotRemovePatient() {
            PatientRegister register = new PatientRegister();
            Patient patientToRemove = new Patient();

            assertFalse(register.removePatient(patientToRemove));
        }

        @Test
        public void removePatient_existingPatientAsArg_removesPatient() {
            PatientRegister register = new PatientRegister();
            Patient patientToRemove = new Patient("12345678910", "Fornavn", "Etternavn", "Diagnose", "Dr. Lege");

            register.addPatient(patientToRemove);
            assertTrue(register.removePatient(patientToRemove));
        }
    }
}
