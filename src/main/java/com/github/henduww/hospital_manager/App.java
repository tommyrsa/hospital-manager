package com.github.henduww.hospital_manager;

import com.github.henduww.hospital_manager.controller.factory.SceneFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Properties;

/**
 * JavaFX application class.
 */
public class App extends Application {
    private static final String TITLE = "Hospital Manager";
    private static final String DATE_CREATED = "14.04.2021";

    /**
     * Main method, responsible for launching a concrete application from a static context.
     * @param args Command line arguments, unused.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Override of {@code Application}'s {@code start()} method.
     * Responsible for loading and running a stage (window) from an FXML file.
     * @param primaryStage Stage object injected by JavaFX.
     */
    @Override
    public void start(Stage primaryStage) {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/MainView.fxml"));
        try {
            Parent root = fxmlLoader.load();

            Scene scene = SceneFactory.create(root);

            primaryStage.setTitle(TITLE);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ignored) {
            // FXML file will always be in jar file, otherwise a test will catch this issue
        }
    }

    /**
     * @return Version of the project, retrieved from Maven POM file.
     */
    public static String getVersion() {
        final Properties properties = new Properties();
        try {
            properties.load(App.class.getResourceAsStream("pom.properties"));
        } catch (IOException ignored) {
            // Properties- and POM file will always be in jar file, otherwise a test will catch this issue
        }

        return properties.getProperty("version");
    }

    /**
     * @return Title of the application.
     */
    public static String getTitle() {
        return TITLE;
    }

    /**
     * @return String representation of the date at which the application was created.
     */
    public static String getDateCreated() {
        return DATE_CREATED;
    }
}
