package com.github.henduww.hospital_manager.model.persistency.serialization;

import com.github.henduww.hospital_manager.controller.factory.AlertFactory;
import com.github.henduww.hospital_manager.model.Patient;
import com.github.henduww.hospital_manager.model.persistency.CSVPersistency;
import com.opencsv.*;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * Deserializer class for deserializing a file into {@code Patient} objects.
 */
public class PatientDeserializer implements CSVPersistency {
    /**
     * Deserializes a given file into {@code Patient} objects.
     * @param file File to deserialize.
     * @return {@code null} if deserialization failed.
     *          List of {@code Patient} objects if deserialization succeeded.
     * @throws IllegalArgumentException If {@code file} is not a CSV file.
     */
    public List<Patient> deserialize(File file) throws IllegalArgumentException {
        Objects.requireNonNull(file);

        if (fileIsNotCSV(file)) {
            throw new IllegalArgumentException("Argument must be a CSV file.");
        }

        try (CSVReader csvReader
            = new CSVReaderBuilder(new FileReader(file, StandardCharsets.UTF_8))
            .withCSVParser(new CSVParserBuilder().withSeparator(';').build())
            .build())
        {
            CsvToBean<Patient> csvToBean = new CsvToBeanBuilder<Patient>(csvReader).withType(Patient.class).build();

            return csvToBean.parse();
        } catch (Exception e) {
            return null;
        }
    }
}
