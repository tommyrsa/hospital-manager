package com.github.henduww.hospital_manager.model.persistency;

import com.github.henduww.hospital_manager.model.Patient;
import org.hibernate.Transaction;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Defines a patient data access object.
 */
public class PatientDAO {

    // Use factory for thread-safety.
    private EntityManagerFactory entityManagerFactory;

    /**
     * Parameterized constructor.
     * @param entityManagerFactory {@code EntityManager} to use for management.
     */
    public PatientDAO(EntityManagerFactory entityManagerFactory) {
        Objects.requireNonNull(entityManagerFactory);

        this.entityManagerFactory = entityManagerFactory;
    }

    /**
     * Default constructor.
     * Private to ensure that {@code entityManager} is always set.
     */
    private PatientDAO() { }

    /**
     * Retrieves every stored {@code Patient} using an explicit SQL statement.
     * @return Every {@code Patient} object currently stored.
     */
    public List<Patient> getAll() {
        EntityManager entityManager = getEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Query query = entityManager.createQuery("SELECT p FROM Patient p", Patient.class);
        transaction.commit();
        List<Patient> queryResult = query.getResultList();

        closeEntityManager(entityManager);
        return queryResult;
    }

    /**
     * Attempts to retrieve a {@code Patient} object using the {@code EntityManager} set in the constructor.
     * @param socialSecurityNumber Social security number of the patient to retrieve.
     * @return {@code Optional} containing the found patient, if any. Empty {@code Optional} if no patient matching this social security number was found.
     */
    public Optional<Patient> get(String socialSecurityNumber) {
        Objects.requireNonNull(socialSecurityNumber, "Argument cannot be null.");
        if (socialSecurityNumber.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument cannot be empty.");
        }

        try {
            EntityManager entityManager = getEntityManager();

            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();

            Patient patient = entityManager.find(Patient.class, socialSecurityNumber);
            transaction.commit();

            closeEntityManager(entityManager);
            return Optional.ofNullable(patient);
        } catch (Exception e) {
            throw new RuntimeException("Unable to get patient by SSN \"" + socialSecurityNumber + "\".");
        }
    }

    /**
     * Checks whether a patient with a given social security number exists.
     * @param socialSecurityNumber Social security number of the patient.
     * @return {@code true} if a patient with the given social security number exists, {@code false} otherwise.
     */
    public boolean exists(String socialSecurityNumber) {
        return get(socialSecurityNumber).isPresent();
    }

    /**
     * Creates a patient entry based on the given patient.
     * @param patient Patient to create an entry for.
     * @throws EntityExistsException If the given patient already exists in the {@code EntityManager} context.
     */
    public void create(Patient patient) throws EntityExistsException {
        Objects.requireNonNull(patient, "Argument cannot be null.");

        if (!exists(patient.getSocialSecurityNumber())) {
            EntityManager entityManager = getEntityManager();

            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(patient);
            transaction.commit();

            closeEntityManager(entityManager);
        } else {
            throw new EntityExistsException("This patient already exists in the database.");
        }
    }

    /**
     * Updates a given patient.
     * @param patient Updated patient.
     * @return Whether or not the update succeeded, based on whether or not the patient existed in the first place.
     */
    public boolean update(Patient patient) {
        Objects.requireNonNull(patient, "Argument cannot be null.");

        if (exists(patient.getSocialSecurityNumber())) {
            EntityManager entityManager = getEntityManager();

            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(patient);
            transaction.commit();

            closeEntityManager(entityManager);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes a given patient.
     * @param socialSecurityNumber Social security number of the patient to delete.
     * @return Whether or not the deletion succeeded, based on whether or not the patient existed in the first place.
     */
    public boolean delete(String socialSecurityNumber) {
        Objects.requireNonNull(socialSecurityNumber, "Argument cannot be null.");
        if (socialSecurityNumber.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument cannot be empty.");
        }

        if (exists(socialSecurityNumber)) {
            EntityManager entityManager = getEntityManager();

            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();

            Patient patient = entityManager.find(Patient.class, socialSecurityNumber);
            entityManager.remove(patient);
            transaction.commit();

            closeEntityManager(entityManager);

            return true;
        } else {
            return false;
        }
    }


    /**
     * @return {@code EntityManager} created from member {@code entityManagerFactory}.
     */
    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    /**
     * Helper method for safely disposing of an {@code EntityManager}.
     * @param entityManager {@code EntityManager} to close.
     */
    private void closeEntityManager(EntityManager entityManager) {
        if (entityManager != null && entityManager.isOpen()) {
            entityManager.close();
        }
    }
}
