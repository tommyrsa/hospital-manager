package com.github.henduww.hospital_manager.model.persistency;

import com.github.henduww.hospital_manager.model.Patient;

import javax.persistence.Persistence;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PatientDBContext {
    private final PatientDAO persistenceUnit;

    public PatientDBContext(String persistenceUnitName) {
        Objects.requireNonNull(persistenceUnitName, "Argument cannot be null.");
        if (persistenceUnitName.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument cannot be empty.");
        }

        this.persistenceUnit = new PatientDAO(Persistence.createEntityManagerFactory(persistenceUnitName));
    }

    public List<Patient> load() {
        return this.persistenceUnit.getAll();
    }

    public void save(List<Patient> patients) {
        Objects.requireNonNull(patients);

        List<Patient> patientsToRemove
            = this.persistenceUnit
            .getAll()
            .stream()
            .filter(patient -> !patients.contains(patient))
            .collect(Collectors.toList());

        patientsToRemove.forEach(patient -> this.persistenceUnit.delete(patient.getSocialSecurityNumber()));

        patients.forEach(patient -> {
            if (this.persistenceUnit.exists(patient.getSocialSecurityNumber())) {
                this.persistenceUnit.update(patient);
            } else {
                this.persistenceUnit.create(patient);
            }
        });
    }
}
