package com.github.henduww.hospital_manager.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Represents a patient register for a hospital.
 */
public class PatientRegister {
    private final List<Patient> patients;

    /**
     * Constructor for a patient register with already existing patients.
     * @param patients Patients to store in the register.
     */
    public PatientRegister(List<Patient> patients) {
        Objects.requireNonNull(patients, "Argument cannot be null.");

        this.patients = patients;
    }

    /**
     * Default constructor for a patient register.
     * Initialises the internal list with a new, empty list.
     */
    public PatientRegister() {
        this.patients = new ArrayList<>();
    }

    /**
     * @return {@code List} object containing the patients stored in the register.
     */
    public List<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Adds a non-null patient to the register.
     * @param patient Patient to add in the register.
     * @return Whether or not the patient could be added to the register, based on whether or not they are already present in the register.
     */
    public boolean addPatient(Patient patient) {
        Objects.requireNonNull(patient, "Argument cannot be null.");

        if (!(this.patients.contains(patient))) {
            this.patients.add(patient);
            return true;
        }

        return false;
    }

    /**
     * Removes a non-null patient from the register.
     * @param patient Patient to be removed from the register.
     * @return Whether or not the patient could be removed from the register, based on whether or not they are present in the register and able to be removed.
     */
    public boolean removePatient(Patient patient) {
        Objects.requireNonNull(patient, "Argument cannot be null.");

        return this.patients.remove(patient);
    }
}
