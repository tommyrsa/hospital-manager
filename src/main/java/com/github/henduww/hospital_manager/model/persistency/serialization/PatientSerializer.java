package com.github.henduww.hospital_manager.model.persistency.serialization;

import com.github.henduww.hospital_manager.controller.factory.AlertFactory;
import com.github.henduww.hospital_manager.model.Patient;
import com.github.henduww.hospital_manager.model.persistency.CSVPersistency;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * Serializer class for serializing {@code Patient} objects to a CSV format.
 */
public class PatientSerializer implements CSVPersistency {
    private final List<Patient> patientsToSerialize;

    /**
     * @param patientsToSerialize List of {@code Patient}s to perform serialization operations on.
     */
    public PatientSerializer(List<Patient> patientsToSerialize) {
        Objects.requireNonNull(patientsToSerialize);

        this.patientsToSerialize = patientsToSerialize;
    }

    /**
     * Encapsulation of default constructor to guarantee that {@code patientsToSerialize} is always set.
     */
    private PatientSerializer() {
        this.patientsToSerialize = null;
    }

    /**
     *
     * @param file CSV file to serialize {@code patientsToSerialize} into.
     * @return Whether or not CSV serialization succeeded.
     * @throws IOException If the file could not be found.
     * @throws IllegalArgumentException If {@code file} is not a CSV file.
     */
    public boolean serialize(File file) throws IOException, IllegalArgumentException {
        Objects.requireNonNull(file);

        if (fileIsNotCSV(file)) {
            throw new IllegalArgumentException("Argument must be a CSV file.");
        }

        try (CSVWriter csvWriter
            = new CSVWriter(
                new FileWriter(file, StandardCharsets.UTF_8),
                ';',
                CSVWriter.NO_QUOTE_CHARACTER,
                ICSVWriter.NO_ESCAPE_CHARACTER,
                ICSVWriter.RFC4180_LINE_END))
        {
            StatefulBeanToCsv<Patient> beanToCsv = new StatefulBeanToCsvBuilder<Patient>(csvWriter).build();

            beanToCsv.write(this.patientsToSerialize);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            return false;
        }

        return true;
    }
}
