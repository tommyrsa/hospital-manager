package com.github.henduww.hospital_manager.model.persistency;

import java.io.File;

/**
 * Common interface for CSV-based serialization and deserialization.
 */
public interface CSVPersistency {
    /**
     * Checks if {@code file} is not a CSV file.
     * @param file File to check extension of.
     * @return Whether or not {@code file} is a CSV file.
     */
    default boolean fileIsNotCSV(File file) {
        String fileName = file.getName();

        return !fileName.substring(fileName.indexOf(".") + 1).equals("csv");
    }
}
