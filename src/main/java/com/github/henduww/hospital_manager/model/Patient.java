package com.github.henduww.hospital_manager.model;

import com.opencsv.bean.CsvBindByName;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Represents a patient admitted to a hospital.
 */
@Entity
public class Patient {
    @CsvBindByName
    @Id
    private final String socialSecurityNumber;

    @CsvBindByName
    private String firstName;

    @CsvBindByName
    private String lastName;

    @CsvBindByName
    private String diagnosis;

    @CsvBindByName
    private String generalPractitioner;

    /**
     * Constructor for creating a new patient object.
     * @param socialSecurityNumber Patient's social security number.
     * @param firstName Patient's first name.
     * @param lastName Patient's last name.
     * @param diagnosis Patient's diagnosis.
     * @param generalPractitioner Name of the general practitioner responsible for the patient.
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) {
        Objects.requireNonNull(socialSecurityNumber, "Argument `socialSecurityNumber` cannot be null.");
        Objects.requireNonNull(firstName, "Argument `firstName` cannot be null.");
        Objects.requireNonNull(lastName, "Argument `lastName` cannot be null.");
        Objects.requireNonNull(diagnosis, "Argument `diagnosis` cannot be null.");
        Objects.requireNonNull(generalPractitioner, "Argument `generalPractitioner` cannot be null.");

        if (!isValidSocialSecurityNumber(socialSecurityNumber)) {
            throw new IllegalArgumentException("Argument `socialSecurityNumber` is not a valid social security number.");
        }

        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Default constructor defined so class is usable as a Java Bean.
     * Sets every field to empty text.
     */
    public Patient() {
        this.socialSecurityNumber = "";
        this.firstName = "";
        this.lastName = "";
        this.diagnosis = "";
        this.generalPractitioner = "";
    }

    /**
     * @return Patient's social security number.
     */
    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    /**
     * @return Patient's first name.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Sets the first name of the patient.
     * @param firstName Patient's first name.
     */
    public void setFirstName(String firstName) {
        this.checkArgIsNullOrEmpty(firstName);

        this.firstName = firstName;
    }

    /**
     * @return Patient's last name.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Sets the last name of the patient.
     * @param lastName Patient's last name.
     */
    public void setLastName(String lastName) {
        this.checkArgIsNullOrEmpty(lastName);

        this.lastName = lastName;
    }

    /**
     * @return Patient's diagnosis.
     */
    public String getDiagnosis() {
        return this.diagnosis;
    }

    /**
     * Sets the diagnosis of the patient.
     * @param diagnosis Patient's diagnosis.
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return Name of the general practitioner responsible for the patient.
     */
    public String getGeneralPractitioner() {
        return this.generalPractitioner;
    }

    /**
     * Sets the name of the general practitioner responsible for the patient.
     * @param generalPractitioner Name of the general practitioner responsible for the patient.
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public static boolean isValidSocialSecurityNumber(String socialSecurityNumber) {
        if (socialSecurityNumber.length() != 11) {
            return false;
        }

        try {
            Long.parseLong(socialSecurityNumber);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Helper method to check if an object is empty.
     * Also checks if a String is empty if it is a String.
     * @param argument Object to check.
     */
    private void checkArgIsNullOrEmpty(Object argument) {
        Objects.requireNonNull(argument, "Argument cannot be null.");

        if (argument instanceof String) {
            if (((String) argument).trim().isEmpty()) {
                throw new IllegalArgumentException("Argument cannot be empty.");
            }
        }
    }

    /**
     * Override of {@code Object}'s {@code equals()} method.
     * Explicitly defines how two patients are considered the same person.
     * @param other Object to compare.
     * @return {@code true} if the object is of type {@code Patient} and they have identical first- and last names, as well an identical social security number.
     *          {@code false} if the objects are not equal.
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) return true;

        if (!(other instanceof Patient)) return false;

        Patient toCompare = (Patient) other;
        return this.firstName.equals(toCompare.firstName)
            && this.lastName.equals(toCompare.lastName)
            && this.socialSecurityNumber.equals(toCompare.socialSecurityNumber);
    }

    /**
     * Override of {@code Object}'s {@code hashCode()} method.
     * Explicitly defines how a patient should be represented as a hashcode.
     * @return Hashcode of the patient.
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(this.socialSecurityNumber)
            * Objects.hashCode(this.firstName)
            * Objects.hashCode(this.lastName);
    }
}
