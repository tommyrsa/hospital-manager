package com.github.henduww.hospital_manager.controller.factory;

import com.github.henduww.hospital_manager.model.Patient;
import javafx.scene.control.Alert;

/**
 * Factory for creating alerts specific to Hospital Manager.
 */
public class AlertFactory {

    /**
     * General factory method for alerts that require no variable information.
     *
     * @param alertType Type of alert to display.
     * @return Alert dialog to display to user.
     */
    public static Alert create(AlertType alertType) {
        Alert.AlertType type;
        String title;
        String header;
        String content;

        switch (alertType) {
            case CANNOT_ADD:
                type = Alert.AlertType.ERROR;
                title = "Failed to add patient";
                header = "This patient already exists";
                content = "A patient with this exact identity (name and social security number) already exists.";
                break;
            case CANNOT_EDIT:
                type = Alert.AlertType.ERROR;
                title = "Could not edit patient";
                header = "Something went wrong while trying to edit patient.";
                content = "Please check that you didn't attempt to edit this patient into an already existing one." +
                        "\nOtherwise, please restart the program and try again.";
                break;
            case CANNOT_DELETE:
                type = Alert.AlertType.ERROR;
                title = "Could not delete patient";
                header = "Something went wrong while trying to delete patient.";
                content = "Please restart the program and try again.";
                break;
            case FILE_ALREADY_OPEN:
                type = Alert.AlertType.INFORMATION;
                title = "File already open";
                header = "This file is already imported";
                content = "The data already present in the program is identical to the one in the file." +
                        "\nPlease select another file to import.";
                break;
            case EMPTY_FIELDS:
                type = Alert.AlertType.ERROR;
                title = "Cannot save";
                header = "One or more input fields are empty";
                content = "Cannot save changes while one or more input fields are empty.";
                break;
            case INVALID_SOCIAL_SECURITY_NUMBER:
                type = Alert.AlertType.ERROR;
                title = "Cannot save";
                header = "Invalid social security number";
                content = "The social security number you input is invalid. Please make sure it consists of 11 digits.";
                break;
            case VERIFY_OVERWRITE:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm overwrite";
                header = "Overwriting a patient means the old one is lost forever.";
                content = "You have made changes to the current patient. " +
                        "Are you sure you would like to save them over the old one?" +
                        "\nClick OK to verify or Cancel to cancel.";
                break;
            case VERIFY_SCRAP_CHANGES:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm scrapping of changes";
                header = "Deleting unsaved changes means they are lost forever.";
                content = "You have made changes to the current patient. Are you sure you would like to scrap them?" +
                        "\nClick OK to verify or Cancel to cancel.";
                break;
            case NO_DATABASE_DATA:
                type = Alert.AlertType.ERROR;
                title = "No data in database";
                header = "The database appears to be empty.";
                content = "The query to the database did not return any elements." +
                        "\nMake sure you have data stored before trying to load " +
                        "it by using the \"Save database\" option.";
                break;
            case DATABASE_OVERWRITE:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm database overwrite";
                header = "Are you sure you would like to overwrite the data in the database?";
                content = "Overwriting the data in the database will cause all previously saved " +
                        "data to be altered by the data currently stored in the program." +
                        "\nClick OK to overwrite or Cancel to cancel.";
                break;
            default:
                throw new IllegalArgumentException(alertType.name() + " is not defined.");
        }

        return createAlert(type, title, header, content);
    }

    /**
     * @param action Attempted action.
     * @return Alert dialog to notify the user they must select a patient from the table before performing {@code action}.
     */
    public static Alert noSelectionAlert(String action) {
        return createAlert(
            Alert.AlertType.ERROR,
            "No selection",
            "No patient was selected!",
            "To " + action.toLowerCase() + " a patient, one must first be selected."
        );
    }

    /**
     * @param patient Patient to be deleted.
     * @return Alert dialog to notify the user about consequences of deleting patients.
     */
    public static Alert deleteConfirmationAlert(Patient patient) {
        return createAlert(
            Alert.AlertType.CONFIRMATION,
            "Delete confirmation",
            "You are about to delete \"" + patient.getLastName() + ", " + patient.getFirstName() + " - " + patient.getSocialSecurityNumber() + "\".",
            "Deleting this patient will permanently remove them and all saved information about them.\nAre you sure you would like to delete \"" + patient.getLastName() + ", " + patient.getFirstName() + " - " + patient.getSocialSecurityNumber() + "\"?\nClick OK to delete or Cancel to cancel."
        );
    }

    /**
     * @param fileName Name of the file with the invalid contents.
     * @return Alert dialog to notify the user that the file they attempted to import has invalid contents.
     */
    public static Alert invalidFileContentsAlert(String fileName) {
        return createAlert(
            Alert.AlertType.ERROR,
            "Invalid file contents",
            "Something is wrong with the contents of your file \"" + fileName + "\".",
            "Check that no required fields are empty (first name, last name or social security number)."
        );
    }

    /**
     * Helper method to create alerts.
     * @param alertType Type of alert.
     * @param title Alert title.
     * @param headerText Header text for alert.
     * @param contentText Alert content.
     * @return Alert dialog to forward.
     */
    private static Alert createAlert(Alert.AlertType alertType,
                                      String title,
                                      String headerText,
                                      String contentText)
    {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        return alert;
    }
}
