package com.github.henduww.hospital_manager.controller;

import com.github.henduww.hospital_manager.controller.factory.AlertFactory;
import com.github.henduww.hospital_manager.controller.factory.AlertType;
import com.github.henduww.hospital_manager.model.Patient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Objects;
import java.util.Optional;

public class ManagePatientController {
    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField socialSecurityNumTextField;

    private boolean isEditing = false;

    private Patient initialPatient = null;

    private Patient finalPatient = null;

    // Cross-stage operations

    /**
     * Sets the dialog in a state where it's aware that editing is taking place.
     * Also sets the initial values of the patient to check for changes.
     * Also fills out the fields with the initial values of the patient.
     * @param patient Patient to edit.
     */
    public void setPatientToEdit(Patient patient) {
        Objects.requireNonNull(patient);

        this.isEditing = true;

        this.firstNameTextField.setText(patient.getFirstName());
        this.lastNameTextField.setText(patient.getLastName());
        this.socialSecurityNumTextField.setText(patient.getSocialSecurityNumber());

        this.initialPatient = patient;
    }

    /**
     * @return Empty optional if no changes were saved.
     *          Optional containing the saved patient if the changes were saved.
     */
    public Optional<Patient> getFinalPatient() {
        return Optional.ofNullable(finalPatient);
    }

    // Listeners

    /**
     * Listener for "Save" button's {@code onAction} event.
     * @param event Event context.
     */
    @FXML
    private void onSave(ActionEvent event) {
        String currentSocialSecurityNumber = this.socialSecurityNumTextField.getText();
        boolean oneFieldIsEmpty = this.firstNameTextField.getText().trim().isEmpty()
                                || this.lastNameTextField.getText().trim().isEmpty()
                                || currentSocialSecurityNumber.trim().isEmpty();

        if (oneFieldIsEmpty) {
            AlertFactory.create(AlertType.EMPTY_FIELDS).showAndWait();
            return;
        }

        if (!Patient.isValidSocialSecurityNumber(currentSocialSecurityNumber))
        {
            AlertFactory.create(AlertType.INVALID_SOCIAL_SECURITY_NUMBER).showAndWait();
            return;
        }

        if (this.hasUnsavedChanges()) {
            if (this.isEditing && !(this.verifiesOverwrite())) {
                return;
            }
        }

        this.finalPatient = this.getCurrentPatient();

        this.closeStage(event);
    }

    /**
     * Listener for "Cancel" button's {@code onAction} event.
     * @param event Event context.
     */
    @FXML
    private void onCancel(ActionEvent event) {
        boolean allFieldsAreEmpty = this.firstNameTextField.getText().trim().isEmpty()
                                    && this.lastNameTextField.getText().trim().isEmpty()
                                    && this.socialSecurityNumTextField.getText().trim().isEmpty();

        if (!(allFieldsAreEmpty)) {
            if (this.hasUnsavedChanges()) {
                if (!(this.verifiesScrapChanges())) {
                    return;
                }
            }
        }

        this.closeStage(event);
    }

    // Helpers

    /**
     * @return Patient object constructed from the values of the fields.
     */
    private Patient getCurrentPatient() {
        String socialSecurityNum = this.socialSecurityNumTextField.getText();
        String firstName = this.firstNameTextField.getText();
        String lastName = this.lastNameTextField.getText();

        return new Patient(socialSecurityNum, firstName, lastName, "", "");
    }

    /**
     * Checks whether or not the user has unsaved changes based on the initial patient value.
     * @return {@code true} if the user has unsaved changes.
     *         {@code false} if the has no unsaved changes.
     */
    private boolean hasUnsavedChanges() {
        String initialSocialSecurityNum = "";
        String initialFirstName = "";
        String initialLastName = "";

        if (this.initialPatient != null) {
            initialSocialSecurityNum = this.initialPatient.getSocialSecurityNumber();
            initialFirstName = this.initialPatient.getFirstName();
            initialLastName = this.initialPatient.getLastName();
        }

        boolean socialSecurityNumberChanged
            = !this.socialSecurityNumTextField.getText().equals(initialSocialSecurityNum);
        boolean firstNameChanged = !this.firstNameTextField.getText().equals(initialFirstName);
        boolean lastNameChanged = !this.lastNameTextField.getText().equals(initialLastName);

        return socialSecurityNumberChanged
                || firstNameChanged
                || lastNameChanged;
    }

    /**
     * Closes the stage, and hence the window of this controller.
     * @param event Event context required to get the source of the event.
     */
    private void closeStage(ActionEvent event) {
        Node source = (Node)event.getSource();
        Stage stage = (Stage)source.getScene().getWindow();

        stage.close();
    }

    // Dialogs

    /**
     * Creates an alert dialog to notify the user about consequences of overwriting patients and allows them to verify or cancel the operation.
     * @return Whether or not the user consents to overwriting the patient with their current changes.
     */
    private boolean verifiesOverwrite() {
        Optional<ButtonType> result
            = AlertFactory
            .create(AlertType.VERIFY_OVERWRITE)
            .showAndWait();

        return result
                .filter(buttonType -> buttonType == ButtonType.OK)
                .isPresent();
    }

    /**
     * Creates an alert dialog to notify the user about consequences of scrapping changes made to patient and allows them to verify or cancel the operation.
     * @return Whether or not the user consents to scrap the changes made to the patient.
     */
    private boolean verifiesScrapChanges() {
        Optional<ButtonType> result
            = AlertFactory
            .create(AlertType.VERIFY_SCRAP_CHANGES)
            .showAndWait();

        return result
                .filter(buttonType -> buttonType == ButtonType.OK)
                .isPresent();
    }
}
