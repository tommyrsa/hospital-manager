package com.github.henduww.hospital_manager.controller.factory;

import javafx.stage.FileChooser;

/**
 * Factory for creating file chooser dialogs for CSV files.
 */
public class CSVFileChooserFactory {
    /**
     * @return File chooser for saving a CSV file.
     */
    public static FileChooser createSave() {
        FileChooser fileChooser = createFileChooser();
        fileChooser.setTitle("Save CSV file");

        return fileChooser;
    }

    /**
     * @return File chooser for opening a CSV file.
     */
    public static FileChooser createOpen() {
        FileChooser fileChooser = createFileChooser();
        fileChooser.setTitle("Edit CSV file");

        return fileChooser;
    }

    /**
     * Helper method to create file chooser with attributes common to both opening and saving.
     * @return FileChooser dialog to forward.
     */
    private static FileChooser createFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV file", "*.csv"));

        return fileChooser;
    }
}
