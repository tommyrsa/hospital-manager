package com.github.henduww.hospital_manager.controller.factory;

public enum AlertType {
    CANNOT_ADD,
    CANNOT_EDIT,
    CANNOT_DELETE,
    FILE_ALREADY_OPEN,
    EMPTY_FIELDS,
    VERIFY_OVERWRITE,
    VERIFY_SCRAP_CHANGES,
    NO_DATABASE_DATA,
    DATABASE_OVERWRITE,
    INVALID_SOCIAL_SECURITY_NUMBER
}
