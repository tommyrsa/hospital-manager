package com.github.henduww.hospital_manager.controller.factory;

import com.github.henduww.hospital_manager.App;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Factory for creating scenes with a common CSS file.
 */
public class SceneFactory {
    /**
     * @param origin Parent object of the caller.
     * @return Scene object injected with CSS file.
     */
    public static Scene create(Parent origin) {
        Scene scene = new Scene(origin);
        scene.getStylesheets().add(App.class.getResource("style.css").toExternalForm());

        return scene;
    }
}
