package com.github.henduww.hospital_manager.controller;

import com.github.henduww.hospital_manager.App;
import com.github.henduww.hospital_manager.controller.factory.AlertType;
import com.github.henduww.hospital_manager.controller.factory.CSVFileChooserFactory;
import com.github.henduww.hospital_manager.controller.factory.SceneFactory;
import com.github.henduww.hospital_manager.controller.factory.AlertFactory;
import com.github.henduww.hospital_manager.model.Patient;
import com.github.henduww.hospital_manager.model.PatientRegister;
import com.github.henduww.hospital_manager.model.persistency.PatientDBContext;
import com.github.henduww.hospital_manager.model.persistency.PatientDAO;
import com.github.henduww.hospital_manager.model.persistency.serialization.PatientDeserializer;
import com.github.henduww.hospital_manager.model.persistency.serialization.PatientSerializer;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.persistence.Persistence;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller for the main window of the application.
 */
public class MainController {

    @FXML
    private Label appStatusLabel;

    @FXML
    private TableView<Patient> patientTable;

    @FXML
    private TableColumn<Patient, String> firstNameColumn;

    @FXML
    private TableColumn<Patient, String> lastNameColumn;

    @FXML
    private TableColumn<Patient, String> socialSecurityNumColumn;

    private final PatientRegister patientRegister = new PatientRegister();

    private final PatientDBContext dbContext = new PatientDBContext("st‐olavs‐register");

    /**
     * JavaFX initializer.
     * Initializes patient table and fills its content, if any.
     * Also sets application status to indicate to the user that the application is ready for use.
     */
    @FXML
    private void initialize() {
        this.patientTable.getItems().addAll(this.patientRegister.getPatients());

        this.patientTable.getItems().addListener(this::patientTableListener);

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.socialSecurityNumColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        this.setAppStatus("Initialized");
    }

    // Listeners

    /**
     * Listener for "About" menu item's {@code onAction} event.
     */
    @FXML
    private void onAboutAction() {
        Alert infoDialog = new Alert(Alert.AlertType.INFORMATION);
        infoDialog.setTitle("Information Dialog - About");

        String headerText = App.getTitle() +
                '\n' +
                App.getVersion();
        infoDialog.setHeaderText(headerText);

        String contentText = "Hospital management software created by" +
                '\n' +
                "© Tommy René Sætre" +
                '\n' +
                App.getDateCreated();
        infoDialog.setContentText(contentText);

        infoDialog.show();
    }

    /**
     * Listener for "Import CSV" menu item's {@code onAction} event.
     */
    @FXML
    private void onImportCSVAction() {
        this.setAppStatus("Importing CSV..");
        FileChooser fileChooser = CSVFileChooserFactory.createOpen();
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
            List<Patient> deserializedPatients = new PatientDeserializer().deserialize(selectedFile);

            if (deserializedPatients == null) {
                AlertFactory.invalidFileContentsAlert(selectedFile.getName()).showAndWait();
                this.setAppStatus("Failed to import");
                return;
            }

            if (deserializedPatients.equals(this.patientRegister.getPatients())) {
                AlertFactory.create(AlertType.FILE_ALREADY_OPEN).showAndWait();
                this.setAppStatus("Canceled import");

                return;
            }

            this.patientTable.getItems().clear();
            this.patientTable.getItems().setAll(deserializedPatients);
            this.setAppStatus("Imported CSV");
        } else {
            this.setAppStatus("Canceled import");
        }
    }

    /**
     * Listener for "Export CSV" menu item's {@code onAction} event.
     * @throws IOException If somehow a file with the exact chosen name appears before it reaches the CSVWriter.
     *                      The use of FileChooser should prevent this entirely.
     */
    @FXML
    private void onExportCSVAction() throws IOException {
        this.setAppStatus("Exporting CSV..");
        FileChooser fileChooser = CSVFileChooserFactory.createSave();
        File selectedFile = fileChooser.showSaveDialog(null);

        if (selectedFile != null) {
            boolean serializeSuccess = new PatientSerializer(this.patientRegister.getPatients()).serialize(selectedFile);

            String appStatusUpdate = serializeSuccess
                                ? "Exported CSV"
                                : "Failed export";

            this.setAppStatus(appStatusUpdate);
        } else {
            this.setAppStatus("Canceled export");
        }
    }

    /**
     * Listener for "Load database" menu item's {@code onAction} event.
     */
    @FXML
    private void onLoadDBAction() {
        this.setAppStatus("Loading data from database..");

        List<Patient> dbResult = this.dbContext.load();

        if (dbResult.size() > 0) {
            this.patientTable.getItems().clear();
            this.patientTable.getItems().setAll(dbResult);

            this.setAppStatus("Loaded data from database");
        } else {
            AlertFactory.create(AlertType.NO_DATABASE_DATA).showAndWait();
            this.setAppStatus("No data retrieved from database");
        }
    }

    /**
     * Listener for "Save database" menu item's {@code onAction} event.
     */
    @FXML
    private void onSaveDBAction() {
        this.setAppStatus("Saving data to database..");

        if (this.userConfirmsDBOverwrite()) {
            this.dbContext.save(this.patientRegister.getPatients());

            this.setAppStatus("Saved data to database");
        } else {
            this.setAppStatus("Cancelled saving to database");
        }
    }

    /**
     * Listener for "Add new patient" menu item's {@code onAction} event.
     */
    @FXML
    private void onAddAction() {
        this.setAppStatus("Adding patient...");

        this.openManagePatientView();
    }

    /**
     * Listener for "Edit selected patient" menu item's {@code onAction} event.
     */
    @FXML
    private void onEditAction() {
        this.setAppStatus("Editing patient...");

        this.getSelectedPatient().ifPresentOrElse(
            this::openManagePatientView,
            () -> {
                this.setAppStatus("No selection");
                AlertFactory.noSelectionAlert("edit").showAndWait();
            }
        );
    }

    /**
     * Listener for "Remove selected patient" menu item's {@code onAction} event.
     */
    @FXML
    private void onRemoveAction() {
        this.getSelectedPatient().ifPresentOrElse(
            patient -> {
                if (this.userConfirmsDelete(patient)) {
                    this.patientTable.getItems().remove(patient);
                    this.setAppStatus("Deleted patient");
                } else {
                    this.setAppStatus("Canceled deletion");
                }
            },
            () -> {
                this.setAppStatus("No selection");
                AlertFactory.noSelectionAlert("delete").showAndWait();
            }
        );
    }

    /**
     * Listener for "Exit" menu item's {@code onAction} event.
     */
    @FXML
    private void onExitAction() {
        System.exit(0);
    }

    /**
     * Listener for {@code labelTable}'s {@code ObservableList}.
     * Detects and handles changes made to to the table's contents.
     * @param change Change context.
     */
    private void patientTableListener(ListChangeListener.Change<? extends Patient> change) {
        // Change list must continuously be updated to prevent loops upon failure to update internal list
        List<Patient> toRemoveFromChange;

        // Look at all the changes that has been made
        while (change.next()) {
            if (change.wasReplaced()) {
                toRemoveFromChange
                    = change
                        .getAddedSubList()
                        .stream()
                        .filter(patient -> this.patientRegister.getPatients().contains(patient))
                        .collect(Collectors.toList());

                if (toRemoveFromChange.size() > 0) {
                    AlertFactory.create(AlertType.CANNOT_EDIT).showAndWait();
                    change.getAddedSubList().removeAll(toRemoveFromChange);
                    continue;
                }
            }
            if (change.wasAdded()) {
                toRemoveFromChange
                        = change
                        .getAddedSubList()
                        .stream()
                        .filter(patient -> !this.patientRegister.addPatient(patient))
                        .collect(Collectors.toList());

                if (toRemoveFromChange.size() > 0) {
                    AlertFactory.create(AlertType.CANNOT_ADD).showAndWait();
                    this.setAppStatus("Failed to add");
                    change.getAddedSubList().removeAll(toRemoveFromChange);
                }
            }
            if (change.wasRemoved()) {
                toRemoveFromChange
                        = change
                        .getRemoved()
                        .stream()
                        .filter(patient -> !this.patientRegister.removePatient(patient))
                        .collect(Collectors.toList());

                if (toRemoveFromChange.size() > 0) {
                    AlertFactory.create(AlertType.CANNOT_DELETE).showAndWait();
                    this.setAppStatus("Failed to delete");
                    change.getRemoved().removeAll(toRemoveFromChange);
                }
            }
        }
    }

    // Helpers

    /**
     * @return Empty optional if no patient in the table is selected.
     *          Optional containing patient if a patient in the table is selected.
     */
    private Optional<Patient> getSelectedPatient() {
        Patient selected = this.patientTable.getSelectionModel().getSelectedItem();
        return (selected == null)
            ? Optional.empty()
            : Optional.of(selected);
    }

    /**
     * Sets the status of the application by setting the text value for {@code appStatusLabel} in the bottom bar.
     * @param status Status of the application to display to the user.
     */
    private void setAppStatus(String status) {
        Objects.requireNonNull(status, "Application status cannot be null.");

        if (status.trim().isEmpty()) {
            throw new IllegalArgumentException("Application status cannot be empty.");
        }

        this.appStatusLabel.setText(status);
    }

    /**
     * Opens the patient manager for editing a specific patient.
     * Replaces the corresponding patient in the table and internal patient register if the user saves their changes.
     * Updates the application status accordingly.
     * @param toEdit Patient to edit.
     */
    private void openManagePatientView(Patient toEdit) {
        Objects.requireNonNull(toEdit, "Argument cannot be null.");

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/ManagePatientView.fxml"));
        try {
            Parent parent = fxmlLoader.load();

            ManagePatientController controller = fxmlLoader.getController();
            controller.setPatientToEdit(toEdit);

            Scene scene = SceneFactory.create(parent);

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Patient Details - Edit");
            stage.setScene(scene);
            stage.showAndWait();

            controller
                .getFinalPatient()
                .ifPresentOrElse(
                    patient -> {
                        int editedPatientIndex = this.patientTable.getItems().indexOf(toEdit);
                        this.patientTable.getItems().set(editedPatientIndex, patient);

                        this.setAppStatus("Edited patient");
                    },
                    () -> this.setAppStatus("Canceled changes")
                );
        } catch (IOException ignored) {
            // FXML file will always be in jar file, otherwise a test will catch this issue
        }
    }

    /**
     * Opens the patient manager for creating a new patient.
     * Adds it to the patient table and internal patient register if the user saves.
     * Updates the application status accordingly.
     */
    private void openManagePatientView() {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/ManagePatientView.fxml"));
        try {
            Parent parent = fxmlLoader.load();

            Scene scene = SceneFactory.create(parent);

            this.setAppStatus("Adding patient...");

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Patient Details - Add");
            stage.setScene(scene);
            stage.showAndWait();

            ManagePatientController controller = fxmlLoader.getController();
            controller
                .getFinalPatient()
                .ifPresentOrElse(
                        patient -> {
                            this.patientTable.getItems().add(patient);
                            this.setAppStatus("Added patient");
                        },
                        () -> this.setAppStatus("Canceled changes")
                );
        } catch (IOException ignored) {
            // FXML file will always be in jar file, otherwise a test will catch this issue
        }
    }

    /**
     * Lets the user decide whether their delete operation was intentional or not.
     * @param patient Patient about to be deleted.
     * @return Whether or not user consents to deleting the patient.
     */
    private boolean userConfirmsDelete(Patient patient) {
        Optional<ButtonType> result
            = AlertFactory
            .deleteConfirmationAlert(patient)
            .showAndWait();

        return result
                .filter(buttonType -> buttonType == ButtonType.OK)
                .isPresent();
    }

    /**
     * Lets the user decide whether they want to overwrite the data in the database or not.
     * @return Whether or not user wants to overwrite database data.
     */
    private boolean userConfirmsDBOverwrite() {
        Optional<ButtonType> result
            = AlertFactory
            .create(AlertType.DATABASE_OVERWRITE)
            .showAndWait();

        return result
                .filter(buttonType -> buttonType == ButtonType.OK)
                .isPresent();
    }
}
