module hospital_manager {
    requires javafx.controls;
    requires javafx.fxml;
    requires opencsv;
    requires java.sql;
    requires java.persistence;
    requires org.hibernate.orm.core;

    opens com.github.henduww.hospital_manager;
    opens com.github.henduww.hospital_manager.controller to javafx.fxml;
    opens com.github.henduww.hospital_manager.model;

    exports com.github.henduww.hospital_manager;
    exports com.github.henduww.hospital_manager.model to opencsv;
}
